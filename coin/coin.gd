extends Area2D

onready var animationPlayer = $AnimationPlayer

func _physics_process(_delta):
	animationPlayer.play("idle")
	
func _on_coin_body_entered(body):
	if body.is_in_group("player"):
		$coinsound.playing = true
		yield(get_tree().create_timer(0.3) , "timeout")
		get_parent().contadorCoins +=1 
		print(get_parent().contadorCoins)
		queue_free()
		





